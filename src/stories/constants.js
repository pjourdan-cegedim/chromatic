export const SEMANTICS = ['primary', 'secondary', 'default', 'success', 'info', 'warning', 'danger'];
export const COMMONS = ['neutral', 'colored'];
export const CONTRASTS = ['light', 'dark'];
export const SIZES = ['sm', 'md', 'lg'];
