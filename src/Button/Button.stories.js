import Button from "../../Button";
import React from "react";
import { action } from '@storybook/addon-actions';
import { CgIcon } from 'common-uitoolkit-icons';
import { withKnobs, select, boolean, radios } from "@storybook/addon-knobs";
import { SEMANTICS, SIZES } from "../constants";

export default {
    title: 'Playground|Button',
    component: Button,
    decorators: [withKnobs]
};
const convertArrayInOptions = (item) => {
    const obj = {};
    obj[item] = item;
    return item;
};
const BEHAVIORS_OPTIONS = SEMANTICS.map(convertArrayInOptions);
const SIZE_OPTIONS = SIZES.map(convertArrayInOptions);

const TYPES = {
    text: 'text',
    icon: 'icon',
    iconText: 'iconText',
};


export const allButtons = () => {
    const type = radios('Type', TYPES, 'text');
    return (
    <div>
        <Button behavior={select('Behavior', BEHAVIORS_OPTIONS, 'primary')} style={{margin: '1rem'}}
                onClick={action(`button-click-${name}`)}
                disabled={boolean("Disabled", false)}
                outline={boolean("Outline", false)}
                outlineNoBorder={boolean("OutlineNoBorder", false)}
                active={boolean("Active", false)}
                circle={boolean("Circle", false)}
                size={select('Size', SIZE_OPTIONS, null)}>
            {type !== 'text' ? <CgIcon name="checked" className={type === 'iconText' ? 'mr-2' : ''}/> : ''}
            {type !== 'icon' ? 'Label' : ''}
        </Button>
    </div>
)};
allButtons.story = {
    name: 'Knobs',
};


export const standardButtons = () => (
    <div>
        <span style={{margin: '1rem'}}>
            <Button behavior="primary" onClick={action('primary')}>Primary</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="secondary">secondary</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="success"><CgIcon name="checked"  className="mr-2"/>success</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="info"><CgIcon name="info"  className="mr-2"/>info</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="warning"><CgIcon name="warning"  className="mr-2"/>warning</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="danger"><CgIcon name="exclamation-circle"  className="mr-2"/>danger</Button>
        </span>
    </div>
);
standardButtons.story = {
    name: 'Standard',
};


export const outlineButtons = () => (
    <div>
        <span style={{margin: '1rem'}}>
            <Button outline behavior="primary" onClick={action('primary')}>Primary</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button  outline behavior="secondary">secondary</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button outline behavior="success"><CgIcon name="checked"  className="mr-2"/>success</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button outline behavior="info"><CgIcon name="info"  className="mr-2"/>info</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button outline behavior="warning"><CgIcon name="warning"  className="mr-2"/>warning</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button outline behavior="danger"><CgIcon name="exclamation-circle"  className="mr-2"/>danger</Button>
        </span>
    </div>
);
outlineButtons.story = {
    name: 'outline',
};

export const disabledButtons = () => (
    <div>
        <span style={{margin: '1rem'}}>
            <Button behavior="primary" disabled onClick={action('primary')}>Primary</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="secondary" disabled>secondary</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="success" disabled><CgIcon name="checked"  className="mr-2"/>success</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="info" disabled><CgIcon name="info"  className="mr-2"/>info</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="warning" disabled><CgIcon name="warning"  className="mr-2"/>warning</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="danger" disabled><CgIcon name="exclamation-circle"  className="mr-2"/>danger</Button>
        </span>
    </div>
);
disabledButtons.story = {
    name: 'Disabled',
};

export const blockButtons = () => (
    <div>
        <span style={{margin: '1rem'}}>
            <Button behavior="primary" block onClick={action('primary')}>Primary</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="secondary" block>secondary</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="success" block><CgIcon name="checked"  className="mr-2"/>success</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="info" block><CgIcon name="info"  className="mr-2"/>info</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="warning" block><CgIcon name="warning"  className="mr-2"/>warning</Button>
        </span>
        <span style={{margin: '1rem'}}>
            <Button behavior="danger" block><CgIcon name="exclamation-circle"  className="mr-2"/>danger</Button>
        </span>
    </div>
);
blockButtons.story = {
    name: 'Block',
};