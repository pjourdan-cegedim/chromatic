import { expect } from 'chai';
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import Button from '../src/components/Button';
import './setup';

describe('Button basic instance', () => {
    const wrapper = shallow(<Button />);
    it('should be mapped to button element', () => {
        expect(wrapper.is('button')).to.equal(true);
    });
});

describe('Button link instance', () => {
    const wrapper = shallow(<Button href="http://test" />);
    it('should be mapped to a element', () => {
        expect(wrapper.is('a')).to.equal(true);
    });
});

describe('Button basic instance with toggle', () => {
    const wrapper = shallow(<Button toggle />);
    it('should be mapped to button element', () => {
        expect(wrapper.is('button')).to.equal(true);
        expect(wrapper.prop('data-toggle')).to.equal('button');
    });
});

describe('Button basic usage', () => {
    const clickStub = sinon.spy();
    const wrapper = shallow(<Button onClick={clickStub} />);
    it('should execute click callback when button is clicked', () => {
        expect(clickStub.calledOnce).to.equal(false);
        const okBtn = wrapper.find('button');
        expect(okBtn.length).to.equal(1);
        okBtn.simulate('click');
        expect(clickStub.calledOnce).to.equal(true);
    });

    it('should not execute click callback when button is disabled and is clicked', () => {
        clickStub.reset();
        wrapper.setProps({ disabled: true });
        expect(clickStub.calledOnce).to.equal(false);
        const okBtn = wrapper.find('button');
        expect(okBtn.length).to.equal(1);
        okBtn.simulate('click', { preventDefault() {} });
        expect(clickStub.calledOnce).to.equal(false);
    });
});

describe('Button with outlineNoBorder parameter', () => {
    const wrapper = shallow(<Button outlineNoBorder />);
    it('should have btn-outline-no-border classname', () => {
        const classnames = wrapper.prop('className').split(' ');
        expect(classnames).to.contain('btn-outline-no-border');
        expect(classnames).to.contain('btn-outline-default');
    });
});
