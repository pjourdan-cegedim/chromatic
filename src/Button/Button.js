import BootstrapButton from 'react-bootstrap/Button'
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './button.scss';

/* ************************************* */
/* ********      VARIABLES      ******** */
/* ************************************* */
const propTypes = {
    /** State of button. Active or not. */
    active: PropTypes.bool,
    /** IgnoreDoc */
    block: PropTypes.bool,
    /** Behavior of button. */
    behavior: PropTypes.oneOf(['default', 'primary', 'secondary', 'success', 'info', 'warning', 'danger', 'link']),
    /** Indicates if the button is a toggle button or not. */
    toggle: PropTypes.bool,
    /** Indicates if the button has a circle shape. */
    circle: PropTypes.bool,
    /** If button is disabled or not. */
    disabled: PropTypes.bool,
    /** If button is outlined or not. */
    outline: PropTypes.bool,
    /** If button is outlined and has no border. */
    outlineNoBorder: PropTypes.bool,
    /** IgnoreDoc */
    tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
    /** IgnoreDoc */
    getRef: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
    /** Callback on click event. */
    onClick: PropTypes.func,
    /** Size of button. */
    size: PropTypes.oneOf(['xs', 'sm', 'md', 'xl']),
    /** IgnoreDoc */
    children: PropTypes.node,
    /** Custom class css. */
    className: PropTypes.string,
    /** IgnoreDoc */
    cssModule: PropTypes.object,
};

const defaultProps = {
    behavior: 'default',
    tag: 'button',
    toggle: false,
    circle: false,
};

class Button extends Component {
    render() {

        const ownProps = {...this.props };
        let className = '';
        if (this.props.behavior) {
            ownProps.variant = this.props.behavior;
            if (this.props.outline || this.props.outlineNoBorder) {
                ownProps.variant = `outline-${this.props.behavior}`;
                if (this.props.outlineNoBorder) {
                    className += 'border-0 '
                }
            }
        }
        if (this.props.circle) {
            className += "rounded-circle ";
        }
        console.log('variant : ', ownProps.variant)
        return <BootstrapButton className={className} {...ownProps} ></BootstrapButton>
    }
}

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

export default Button;
