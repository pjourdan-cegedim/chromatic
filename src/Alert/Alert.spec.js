import { expect } from 'chai';
import React from 'react';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';
import Alert from '../../src/components/Alert/Alert';
import '../setup';

describe('Alert basic instance', () => {
    const wrapper = shallow(<Alert headline={'test'} behavior="info"><div >test</div></Alert>);
    it('should be mapped to div element', () => {
        wrapper.instance().componentDidMount();
        expect(wrapper.is('div')).to.equal(true);
    });
});

describe('Alert with details instance', () => {
    const details = ['toto'];
    const wrapper = shallow(<Alert details={details} showIcon={false}><div>test</div></Alert>);
    it('should be mapped to div element', () => {
        wrapper.instance().componentDidMount();
        expect(wrapper.is('div')).to.equal(true);
        expect(wrapper.find('.alert').html().includes('fa-caret-down')).to.equal(true);
        expect(wrapper.find('.alert').html().includes('collapse show')).to.equal(false);
        wrapper.find('.alert').simulate('click');
        expect(wrapper.find('.alert').html().includes('fa-caret-up')).to.equal(true);
        expect(wrapper.find('.alert').html().includes('collapse show')).to.equal(true);
        wrapper.find('.alert').simulate('keypress');
        expect(wrapper.find('.alert').html().includes('fa-caret-down')).to.equal(true);
        expect(wrapper.find('.alert').html().includes('collapse show')).to.equal(false);

    });
});

describe('Alert basic dismiss usage', () => {
    const onDismissStub = sinon.spy();

    const wrapper = shallow(<Alert onDismiss={onDismissStub} isDismissable showIcon>test</Alert>);

    it('should have a correct class when behavior props changes', () => {
        expect(
            wrapper
                .find('.alert-icon')
                .html()
                .includes('fa-info'),
        ).to.equal(true);
        wrapper.setProps({ behavior: 'danger' });
        expect(
            wrapper
                .find('.alert-icon')
                .html()
                .includes('fa-exclamation-circle'),
        ).to.equal(true);
        wrapper.setProps({ behavior: 'success' });
        expect(
            wrapper
                .find('.alert-icon')
                .html()
                .includes('fa-check'),
        ).to.equal(true);
        wrapper.setProps({ behavior: 'warning' });
        expect(
            wrapper
                .find('.alert-icon')
                .html()
                .includes('fa-exclamation'),
        ).to.equal(true);
    });

    it('should execute onDismiss callback when button is clicked', () => {
        expect(onDismissStub.calledOnce).to.equal(false);
        expect(wrapper.find('.alert-icon').length).to.equal(1);
        wrapper.instance().componentDidMount();
        const closeBtn = wrapper.find('button');
        expect(closeBtn.length).to.equal(1);
        closeBtn.simulate('click', {
            preventDefault: () => {},
            stopPropagation: () => {}
        });
        expect(onDismissStub.calledOnce).to.equal(true);
    });
});

describe('Alert basic timeout usage', () => {
    let clock;

    before(() => {
        clock = sinon.useFakeTimers();
    });

    after(() => {
        clock.restore();
    });

    const onDismissStub = sinon.spy();

    const wrapper = shallow(<Alert timeout={1} onDismiss={onDismissStub} />);

    it('should execute onDismiss callback when timeout is reached', () => {
        onDismissStub.reset();
        expect(onDismissStub.calledOnce).to.equal(false);
        wrapper.instance().componentDidMount();

        clock.tick(100);
        expect(onDismissStub.calledOnce).to.equal(true);
        wrapper.instance().componentWillUnmount();
    });
});
