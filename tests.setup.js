import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { JSDOM } from 'jsdom';
import sinon from 'sinon';
import { $, jQuery } from 'jquery';

const requests = [];
const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
const { window } = jsdom;

configure({ adapter: new Adapter() });

function copyProps(src, target) {
    const props = Object.getOwnPropertyNames(src)
        .filter(prop => typeof target[prop] === 'undefined')
        .map(prop => Object.getOwnPropertyDescriptor(src, prop));
    Object.defineProperties(target, props);
}

global.window = window;
global.document = window.document;
global.$ = $;
global.jQuery = jQuery;

global.navigator = {
    userAgent: 'node.js',
};
copyProps(window, global);

global.window.URL = {
    revokeObjectURL() {
        // fake method
    },
    createObjectURL() {
        // fake method
    },
};

global.XMLHttpRequest = sinon.useFakeXMLHttpRequest();

global.XMLHttpRequest.onCreate = xhr => {
    requests.push(xhr);
};

global.Blob = function blob() {
    // fake method
};
global.Blob.prototype = {};
global.location = {
    href: {
        replace() {
            return '';
        },
    },
};

global.window.matchMedia =
    global.window.matchMedia ||
    function() {
        return {
            matches: false,
            addListener: () => {},
            removeListener: () => {},
        };
    };
global.getComputedStyle = function getComputedStyle() {
    return { position: 'fixed' };
};
global.pageYOffset = 10;
global.pageXOffset = 10;
global.innerHeight = 10;
global.innerWidth = 10;

global.MutationObserver = function mutationObserver() {
    return { observe() {} };
};
global.MutationObserver.prototype = {};

global.FileReader = function fileReader() {
    return { onload() {}, readAsDataURL() {} };
};
global.FileReader.prototype = {};

global.window.HTMLElement.prototype.focus = function() {
    // fake method
};
global.window.HTMLElement.prototype.blur = function() {
    // fake method
};

global.requestAnimationFrame = function() {
    // fake method
};

export { requests as default };
